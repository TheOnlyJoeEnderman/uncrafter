local S = minetest.get_translator("uncrafter")
local uncrafting_cannot_message = S("This item can't be uncrafted, or no uncrafting recipe has been set!")
local uncrafting_table_label = S("Uncrafting Table")
local uncrafting_button_label = S("Uncraft Item")
allow_items = {
    "default:torch",
    ------Diamant/Diamond----------
    "default:shovel_diamond",
    "default:pick_diamond",
    "default:axe_diamond",
    "default:sword_diamond",
    ------Bois/Wood-------------
    "default:pick_wood",
    "default:sword_wood",
    "default:axe_wood",
    "default:shovel_wood",
    ------Fer/Steel----------
    "default:pick_steel",
    "default:axe_steel",
    "default:sword_steel",
    "default:shovel_steel",
    --------Pierre/Stone------
    "default:pick_stone",
    "default:sword_stone",
    "default:axe_stone",
    "default:shovel_stone",
    -------Bronze------
    "default:pick_bronze",
    "default:axe_bronze",
    "default:sword_bronze",
    "default:shovel_bronze",
    ------Mese----------
    "default:pick_mese",
    "default:axe_mese",
    "default:sword_mese",
    "default:shovel_mese"
    ------Your Item-----
}


local tabledef = {

	description = uncrafting_table_label,

    groups = {cracky = 2, choppy = 2, oddly_breakable_by_hand = 1},

	tiles = {
		"uncrafter_top.png","uncrafter_bottom.png",
		"uncrafter_side.png", "uncrafter_side.png",
		"uncrafter_back.png", "uncrafter_front.png"
	},

    paramtype2 = "facedir",

	sounds = default.node_sound_metal_defaults(),


	can_dig = function(pos, player)

		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()

		if inv:is_empty("slot") then

			if not minetest.is_protected(pos, player:get_player_name()) then
				return true
			end
		end
	end,

	allow_metadata_inventory_put = function(pos, listname, index, stack, player)
		return stack:get_count()
	end,

	allow_metadata_inventory_take = function(pos, listname, index, stack, player)
		return stack:get_count()
	end,

    on_receive_fields = function(pos, formname, fields, sender)
        if fields.bouton then
            local meta = minetest.get_meta(pos)
            local inv = meta:get_inventory()
            local playerInv = sender:get_inventory()
            local playerName = sender:get_player_name()
            local input = inv:get_stack("slot", 1):get_name()
            local craft = minetest.get_craft_recipe(input)
            
            if craft.items == nil or input == "" or table_contains(allow_items, input) == false then     
                minetest.chat_send_player(playerName, uncrafting_cannot_message)
            else    
                for i=1, 9 do
                    if craft.items[i] == "group:stick" then 
                        craft.items[i] = "default:stick"
                    end
                    if craft.items[i] == "group:wood" then 
                        craft.items[i] = "default:wood"
                    end
                    if craft.items[i] == "group:stone" then 
                        craft.items[i] = "default:cobble"
                    end
                    itemStackToAdd = playerInv:add_item("main", craft.items[i])
                    if not itemStackToAdd:is_empty() then
                        local pos_drop = minetest.get_player_by_name(playerName):get_pos()
                        minetest.spawn_item(pos_drop, craft.items[i]) 
                    end
                end
                inv:set_stack("slot", 1, "")
            end
        end
    end,

	on_construct = function(pos)

		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()

		inv:set_size("slot", 1)

		meta:set_string("formspec",
    		"size[8.5,10]" ..
    		"list[current_player;main;0.2,6;8,4;0]" ..
    		"list[context;slot;3.8,2;1,1]" ..
    		"label[3.3,0.4;" .. uncrafting_table_label .. "]" ..
    		"button[2.8,4;3,0.8;bouton;" .. uncrafting_button_label .. "]" ..
    		"listring[context;slot]")
    end,

	on_blast = function() end
}

minetest.register_alias("uncraft:uncrafttable", "uncrafter:uncrafter")

minetest.register_craft({
	output = "uncrafter:uncrafter",
	recipe = {
		{"default:steel_ingot", "default:steel_ingot"},
		{"group:wood", "group:wood"},
		{"group:wood", "group:wood"}
	}
})

minetest.register_node("uncrafter:uncrafter", tabledef)

function table_contains(table, element)
    for _, value in pairs(table) do
      if value == element then
        return true
      end
    end
    return false
  end
